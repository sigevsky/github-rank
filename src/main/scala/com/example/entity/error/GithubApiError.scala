package com.example.entity.error

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder
import org.http4s.DecodeFailure

sealed abstract class GithubApiError(message: String) extends Exception {
  final override def fillInStackTrace(): Throwable = this
  final override def getMessage(): String          = message
}

object GithubApiError {

  final case class UnhandledResponseError(
      message: String,
      code: Int
  ) extends GithubApiError(message) {
    override def toString(): String = s"UnhandledResponseError($message, $code)"
  }

  final case class UnknownError(message: String) extends GithubApiError(message) {
    override def toString(): String = s"UnhandledResponseError($message)"
  }

  final case class JsonParsingError(
      message: String,
      cause: Option[Throwable]
  ) extends GithubApiError(message) {
    override def toString(): String    = s"JsonParsingError($message, $cause)"
    override def getCause(): Throwable = cause.orNull
  }

  final case class BasicError(
      message: String
  ) extends GithubApiError(message) {
    override def toString(): String = s"BasicError($message)"
  }

  final case class BadRequestError(
      message: String
  ) extends GithubApiError(message) {
    override def toString(): String = s"BadRequestError($message)"
  }

  final case class UnauthorizedError(
      message: String,
      documentation_url: String
  ) extends GithubApiError(message) {
    override def toString(): String = s"UnauthorizedError($message, $documentation_url)"
  }

  final case class ForbiddenError(
      message: String,
      documentation_url: String
  ) extends GithubApiError(message) {
    override def toString(): String = s"ForbiddenError($message, $documentation_url)"
  }

  final case class NotFoundError(
      message: String,
  ) extends GithubApiError(message) {
    override def toString(): String = s"NotFoundError($message)"
  }

  final case class UnprocessableEntityError(
      message: String,
      errors: List[String]
  ) extends GithubApiError(message) {
    override def toString(): String = s"UnprocessableEntityError($message, $errors)"
  }

  final case class RateLimitExceededError(
      message: String,
      documentation_url: String
  ) extends GithubApiError(message) {
    override def toString(): String = s"RateLimitExceededError($message, $documentation_url)"
  }

  final case class FailedToParseUri(message: String, details: String) extends GithubApiError(message)

  object JsonParsingError {
    def apply(df: DecodeFailure): JsonParsingError = JsonParsingError(df.message, df.cause)
  }

  object BadRequestError {
    private[example] implicit val badRequestErrorDecoder: Decoder[BadRequestError] =
      deriveDecoder[BadRequestError]
  }

  object UnauthorizedError {
    private[example] implicit val unauthorizedErrorDecoder: Decoder[UnauthorizedError] =
      deriveDecoder[UnauthorizedError]
  }

  object ForbiddenError {
    private[example] implicit val forbiddenErrorDecoder: Decoder[ForbiddenError] =
      deriveDecoder[ForbiddenError]
  }

  object NotFoundError {
    private[example] implicit val notFoundErrorDecoder: Decoder[NotFoundError] =
      deriveDecoder[NotFoundError]
  }

  object UnprocessableEntityError {
    private[example] implicit val uEntityErrorDecoder: Decoder[UnprocessableEntityError] =
      deriveDecoder[UnprocessableEntityError]
  }

  object RateLimitExceededError {
    private[example] implicit val rleErrorDecoder: Decoder[RateLimitExceededError] =
      deriveDecoder[RateLimitExceededError]
  }
}
