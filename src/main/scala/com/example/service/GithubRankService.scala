package com.example.service

import com.example.entity.error.GithubApiError
import com.example.entity.payload.{User, UserContrib}
import logstage.LogIO3
import zio.{IO, ZIO, stream}

trait GithubRankService {
  def rankContributors(org: String): IO[GithubApiError, List[UserContrib]]
}

object GithubRankService {
  final class Impl(githubService: GithubService, log: LogIO3[ZIO]) extends GithubRankService {
    override def rankContributors(org: String): IO[GithubApiError, List[UserContrib]] = {
      log.info(s"Fetching contributors for organization: $org") *> githubService
        .fetchRepos(org)
        .flatMapPar(4, 16)(repo => githubService.fetchContributors(repo))
        .collect { case User(_, lg, Some(contrib)) => UserContrib(lg, contrib) }
        .groupBy(user => ZIO.succeed((user.login, user.contributions))) { case (key, group) =>
          stream.Stream.fromEffect(group.fold(0L)((acc, v) => acc + v).map(UserContrib(key, _)))
        }
        .runCollect
        .map(_.sortBy(_.contributions)(Ordering[Long].reverse).take(100).toList)
    }
  }
}
