package com.example.config

import org.http4s.Header
import org.http4s.headers.Accept

final case class GithubConfig(
    baseUrl: String,
    authorizeUrl: String,
    accessTokenUrl: String,
    headers: Map[String, String]
) {
  def toHeaderList: List[Header] = headers.map { case (k, v) => Header(k, v) }.toList
}

object GithubConfig {
  val default: GithubConfig =
    GithubConfig(
      baseUrl = "https://api.github.com/",
      authorizeUrl = "https://github.com/login/oauth/authorize?client_id=%s&redirect_uri=%s&scope=%s&state=%s",
      accessTokenUrl = "https://github.com/login/oauth/access_token",
      headers = Map(Accept.name.value -> "application/vnd.github.v3+json")
    )
}
