package com.example.api

import cats.effect.Sync
import com.example.entity.payload.UserContribResp
import com.example.service.GithubRankService
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import zio._

final class GithubRankApi(dsl: Http4sDsl[Task], rankService: GithubRankService)(implicit S: Sync[Task])
    extends HttpApi {
  import dsl._

  override val name: String = "rank-api"

  override def http: HttpRoutes[Task] = HttpRoutes.of { case GET -> Root / "org" / org / "contributors" =>
    Ok(rankService.rankContributors(org).map(UserContribResp.apply))
  }
}
