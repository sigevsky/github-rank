inThisBuild(
  Seq(
    scalaVersion := "2.13.4",
    version      := "1.0.0",
    organization := "com.example",
  )
)

val V = new {
  val http4sVersion = "0.21.19"
  val circeVersion = "0.12.3"
  val izumiVersion = "1.0.3"
  val zioVersion = "1.0.4-2"
  val interopCatsVersion = "2.1.4.0"
}

lazy val githubRank = project
  .in(file("."))
  .settings(
    name := "github-rank",
    libraryDependencies ++=Seq(
      "io.circe" %% "circe-core" % V.circeVersion,
      "io.circe" %% "circe-generic" % V.circeVersion,
      "io.circe" %% "circe-parser" % V.circeVersion,
      "dev.zio" %% "zio" % V.zioVersion,
      "dev.zio" %% "zio-interop-cats" % V.interopCatsVersion,
      "dev.zio" %% "zio-streams" % V.zioVersion,
      "org.http4s" %% "http4s-blaze-client" % V.http4sVersion,
      "org.http4s" %% "http4s-blaze-server" % V.http4sVersion,
      "org.http4s" %% "http4s-dsl" % V.http4sVersion,
      "org.http4s" %% "http4s-circe" % V.http4sVersion,
      "io.7mind.izumi" %% "distage-core" % V.izumiVersion,
      "io.7mind.izumi" %% "distage-framework" % V.izumiVersion,
      "io.7mind.izumi" %% "logstage-core" % V.izumiVersion,
      "io.7mind.izumi" %% "logstage-rendering-circe" % V.izumiVersion,
      "io.7mind.izumi" %% "logstage-adapter-slf4j" % V.izumiVersion,
      "io.7mind.izumi" %% "distage-extension-logstage" % V.izumiVersion,
      "io.7mind.izumi" %% "logstage-sink-slf4j" % V.izumiVersion
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.11.3" cross CrossVersion.full),
    scalacOptions -= "-Xfatal-warnings",
    scalacOptions += "-Wconf:msg=kind-projector:silent",
    scalacOptions ++= Seq(
      s"-Xmacro-settings:product-name=${name.value}",
      s"-Xmacro-settings:product-version=${version.value}",
      s"-Xmacro-settings:product-group=${organization.value}",
      s"-Xmacro-settings:scala-version=${scalaVersion.value}",
      s"-Xmacro-settings:scala-versions=${crossScalaVersions.value.mkString(":")}",
      s"-Xmacro-settings:sbt-version=${sbtVersion.value}",
      s"-Xmacro-settings:git-repo-clean=${git.gitUncommittedChanges.value}",
      s"-Xmacro-settings:git-branch=${git.gitCurrentBranch.value}",
      s"-Xmacro-settings:git-described-version=${git.gitDescribedVersion.value.getOrElse("")}",
      s"-Xmacro-settings:git-head-commit=${git.gitHeadCommit.value.getOrElse("")}",
    ),
    mainClass in assembly := Some("com.example.MainBase"),
    test in assembly := {},
  )