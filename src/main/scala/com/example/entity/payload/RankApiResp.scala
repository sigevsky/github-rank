package com.example.entity.payload

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import org.http4s.EntityEncoder
import org.http4s.circe.jsonEncoderOf
import zio.Task

final case class UserContrib(login: String, contributions: Long)

object UserContrib {
  implicit val decoderRankPayload: Decoder[UserContrib] = deriveDecoder[UserContrib]
  implicit val encoderRankPayload: Encoder[UserContrib] = deriveEncoder[UserContrib]

  implicit val loginEntityEncoder: EntityEncoder[Task, UserContrib] = jsonEncoderOf[Task, UserContrib]
}

final case class UserContribResp(contributors: List[UserContrib])

object UserContribResp {
  implicit val encodeUserContribResp: Encoder[UserContribResp]                = deriveEncoder[UserContribResp]
  implicit val entEncodeUserContribResp: EntityEncoder[Task, UserContribResp] = jsonEncoderOf[Task, UserContribResp]
}
