package com.example.entity.payload

import io.circe.Decoder
import io.circe.generic.semiauto._

final case class User(
    id: Long,
    login: String,
    contributions: Option[Long] = None
)

object User {
  implicit val contributorRespDec: Decoder[User] = deriveDecoder[User]
}

final case class Repository(
    id: Long,
    name: String,
    full_name: String,
    owner: User,
    `private`: Boolean,
    fork: Boolean,
    archived: Boolean,
    createdAt: String,
    updatedAt: String,
    pushedAt: String,
    contributorsUrl: String
)

object Repository {
  implicit val decodeRepositoryBase: Decoder[Repository] = {
    Decoder.instance { c =>
      for {
        id               <- c.downField("id").as[Long]
        name             <- c.downField("name").as[String]
        full_name        <- c.downField("full_name").as[String]
        owner            <- c.downField("owner").as[User]
        priv             <- c.downField("private").as[Boolean]
        fork             <- c.downField("fork").as[Boolean]
        archived         <- c.downField("archived").as[Boolean]
        created_at       <- c.downField("created_at").as[String]
        updated_at       <- c.downField("updated_at").as[String]
        pushed_at        <- c.downField("pushed_at").as[String]
        contributors_url <- c.downField("contributors_url").as[String]
      } yield Repository(
        id = id,
        name = name,
        full_name = full_name,
        owner = owner,
        `private` = priv,
        fork = fork,
        archived = archived,
        createdAt = created_at,
        updatedAt = updated_at,
        pushedAt = pushed_at,
        contributorsUrl = contributors_url
      )
    }
  }
}
