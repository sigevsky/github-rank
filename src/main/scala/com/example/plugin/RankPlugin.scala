package com.example.plugin

import com.example.RankRole
import com.example.api.{GithubRankApi, HttpApi}
import com.example.config.{GithubConfig, GithubToken}
import com.example.middleware.{CachingMiddleware, ErrorMiddleware, Middleware}
import com.example.service.{GithubRankService, GithubService}
import distage.plugins.PluginDef
import izumi.distage.model.definition.ModuleDef
import izumi.distage.roles.model.definition.RoleModuleDef
import izumi.logstage.api.IzLogger
import izumi.logstage.api.Log.Level.Trace
import izumi.logstage.distage.LogIO3Module
import logstage.ConsoleSink
import org.http4s.client.Client
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Server
import zio.{Task, ZIO}

final class RankPlugin extends PluginDef {
  include(LogIO3Module[ZIO]())
  include(common)
  include(githubApi)
  include(githubRole)

  def githubApi: ModuleDef = new ModuleDef {
    make[GithubRankService].from[GithubRankService.Impl]
    make[GithubService].from[GithubService.Impl]
    make[GithubConfig].fromValue(GithubConfig.default)
    make[GithubToken].fromEffect(ZIO.effect(sys.env.get("GH_TOKEN")).map(GithubToken))
    make[GithubRankApi]
    many[HttpApi].weak[GithubRankApi]
    many[Middleware]
      .add[ErrorMiddleware]
      .add[CachingMiddleware]
  }

  def common: ModuleDef = new ModuleDef {
    make[Http4sDsl[Task]]
    make[Client[Task]].fromResource[HttpComponents.HttpClient]
    make[Server[Task]].fromResource[HttpComponents.BlazeServer]
    make[IzLogger].fromValue {
      val textSink = ConsoleSink.text(colored = true)
      //    val jsonSink = ConsoleSink(LogstageCirceRenderingPolicy(prettyPrint = true))
      val sinks    = List(textSink)
      IzLogger(Trace, sinks)
    }
  }

  def githubRole: ModuleDef = new RoleModuleDef {
    makeRole[RankRole]
  }
}
