package com.example.middleware

import cats.Applicative
import cats.data.{Kleisli, OptionT}
import com.example.entity.error.GithubApiError._
import com.example.entity.error.{ErrorResp, GithubApiError}
import logstage.LogIO3
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes, Response}
import zio.{Task, ZIO}

final class ErrorMiddleware(dsl: Http4sDsl[Task], logIO3: LogIO3[ZIO])(implicit A: Applicative[Task]) extends Middleware {
  import dsl._

  override val order: Int = 0

  override def middle(route: HttpRoutes[Task]): HttpRoutes[Task] = Kleisli { req =>
    OptionT(route(req).value.catchAll {
      case gh: GithubApiError => ghtToApiError(gh).map(Some.apply)
      case err: Throwable     =>
        logIO3.error(s"Failed due to ${err.getMessage}") *>
          InternalServerError(ErrorResp(500, "unknown", "rank")).map(Some.apply)
    })
  }

  private def ghtToApiError: GithubApiError => Task[Response[Task]] = {
    case ForbiddenError(msg, _)           => Forbidden(ErrorResp(403, msg, "github"))
    case BadRequestError(msg)             => BadRequest(ErrorResp(400, msg, "github"))
    case UnauthorizedError(msg, _)        => BadRequest(ErrorResp(400, msg, "github"))
    case UnprocessableEntityError(msg, _) => UnprocessableEntity(ErrorResp(422, msg, "github"))
    case RateLimitExceededError(msg, _)   => Locked(ErrorResp(423, msg, "github"))
    case NotFoundError(msg)               => NotFound(ErrorResp(404, msg, "github"))
    case JsonParsingError(msg, _)         => InternalServerError(ErrorResp(500, msg, "github"))
    case UnknownError(msg)                => InternalServerError(ErrorResp(500, msg, "github"))
    case UnhandledResponseError(msg, _)   => InternalServerError(ErrorResp(500, msg, "rank"))
    case _                                => InternalServerError(ErrorResp(500, "unknown", "rank"))
  }
}
