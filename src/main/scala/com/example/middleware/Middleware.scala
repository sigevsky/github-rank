package com.example.middleware

import org.http4s.HttpRoutes
import zio.Task

trait Middleware {
  val order: Int = Int.MaxValue
  def middle(route: HttpRoutes[Task]): HttpRoutes[Task]
}
