package com.example.common

import com.example.entity.error.GithubApiError
import org.http4s.util.CaseInsensitiveString
import org.http4s.{Headers, Uri}
import zio.stream.Stream
import zio.{Chunk, IO, ZIO}

import scala.util.matching.Regex

object CommonUtils {
  private val link: Regex = """<(.*?)>; rel="(\w+)"""".r

  def autoPage[T](first: Uri)(call: Uri => IO[GithubApiError, (Headers, List[T])]): Stream[GithubApiError, T] = {
    val chunker: Option[Uri] => IO[GithubApiError, Option[(Chunk[T], Option[Uri])]] = {
      case Some(uri) =>
        call(uri).map(res => Option(Chunk.fromIterable(res._2) -> getNext(res._1)))
      case None      => ZIO.succeed(None)
    }
    Stream.unfoldChunkM[Any, GithubApiError, T, Option[Uri]](Some(first))(chunker)
  }

  private def getNext(headers: Headers): Option[Uri] = {
    val linkHeader = headers.toList
      .find(_.name.equals(CaseInsensitiveString("link")))
      .map(_.value)

    for {
      hd  <- linkHeader
      url <- link.findAllMatchIn(hd).collectFirst { case link(url, relation) if relation == "next" => url }
      uri <- Uri.fromString(url).toOption
    } yield uri
  }
}
