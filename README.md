### How to run

If you need to support scanning large organization, go and get github api token
and put it into `GH_TOKEN` environment variable

1. Build an image: `docker build -t github-rank .`
2. Run the image `docker run -d -p 8080:8080 -e GH_TOKEN=$GH_TOKEN github-rank:latest`
3. Verify api is working correctly `curl -X GET http://localhost:8080/org/idris-lang/contributors`
4. Stop application `docker stop <container-name>`