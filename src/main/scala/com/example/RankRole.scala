package com.example

import com.example.api.GithubRankApi
import izumi.distage.model.definition.Lifecycle
import izumi.distage.plugins.PluginConfig
import izumi.distage.roles.RoleAppMain
import izumi.distage.roles.model.{RoleDescriptor, RoleService}
import izumi.fundamentals.platform.cli.model.raw.{RawEntrypointParams, RawRoleParams}
import logstage.LogIO3
import org.http4s.server.Server
import zio.{Task, ZIO}

import scala.annotation.unused

final class RankRole(@unused runningServer: Server[Task], @unused api: GithubRankApi, log: LogIO3[ZIO])
    extends RoleService[Task] {
  override def start(roleParameters: RawEntrypointParams, freeArgs: Vector[String]): Lifecycle[Task, Unit] =
    Lifecycle.liftF(log.info("Rank api is up!"))
}

object RankRole extends RoleDescriptor {
  final val id = "rank"
}

object MainBase extends RoleAppMain.LauncherBIO3[ZIO] {
  override def requiredRoles(argv: RoleAppMain.ArgV): Vector[RawRoleParams] = {
    Vector(RawRoleParams(RankRole.id))
  }

  override protected def pluginConfig: PluginConfig = PluginConfig.cached(pluginsPackage = "com.example.plugin")
}
