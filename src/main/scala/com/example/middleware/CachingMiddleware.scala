package com.example.middleware
import cats.MonadThrow
import cats.data.Kleisli
import cats.effect.Timer
import org.http4s.HttpRoutes
import org.http4s.server.middleware.Caching
import zio.Task

import scala.concurrent.duration._
import scala.language.postfixOps

final class CachingMiddleware(implicit Tm: Timer[Task], Mt: MonadThrow[Task]) extends Middleware {
  override def middle(http: HttpRoutes[Task]): HttpRoutes[Task] = Kleisli { req =>
    http(req).flatMapF(resp =>
      Caching.publicCacheResponse(5 minutes)(resp).map(Some(_)))
  }
}
