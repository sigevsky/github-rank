package com.example.api

import org.http4s.HttpRoutes
import zio.Task

trait HttpApi {
  val name: String
  def http: HttpRoutes[Task]
}
