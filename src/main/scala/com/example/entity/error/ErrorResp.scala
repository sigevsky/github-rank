package com.example.entity.error

import io.circe.Encoder
import io.circe.derivation.deriveEncoder
import org.http4s.EntityEncoder
import org.http4s.circe.jsonEncoderOf
import zio.Task

case class ErrorResp(code: Int, message: String, origin: String)

object ErrorResp {
  implicit val encodeErrorResp: Encoder[ErrorResp]                = deriveEncoder[ErrorResp]
  implicit val errorRespEntityEnc: EntityEncoder[Task, ErrorResp] = jsonEncoderOf[Task, ErrorResp]
}
