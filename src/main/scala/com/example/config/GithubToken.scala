package com.example.config

final case class GithubToken(token: Option[String])
