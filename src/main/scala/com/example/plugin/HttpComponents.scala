package com.example.plugin

import cats.effect.{ConcurrentEffect, Timer}
import cats.implicits._
import com.example.api.HttpApi
import com.example.middleware.Middleware
import distage.Id
import izumi.distage.model.definition.Lifecycle
import logstage.LogIO3
import org.http4s.HttpRoutes
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.server.Server
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.syntax.kleisli._
import zio.{Task, ZIO}

import scala.concurrent.ExecutionContext

object HttpComponents {

  final class HttpClient(cpuPool: ExecutionContext @Id("zio.cpu"), log: LogIO3[ZIO])(implicit concurrentEffect: ConcurrentEffect[Task])
      extends Lifecycle.Of[ZIO[Any, Throwable, *], Client[Task]](
        Lifecycle.fromCats {
          BlazeClientBuilder[Task](cpuPool).resource.onFinalize(log.info("Done finalizing pool"))
        }
      )

  final class BlazeServer(
      middlewares: Set[Middleware],
      allHttpApis: Set[HttpApi],
      cpuPool: ExecutionContext @Id("zio.cpu"),
      log: LogIO3[ZIO]
  )(implicit
      concurrentEffect: ConcurrentEffect[Task],
      timer: Timer[Task],
  ) extends Lifecycle.Of[ZIO[Any, Throwable, *], Server[Task]](
        Lifecycle.fromCats {
          val combinedApis = allHttpApis.map(_.http).toList.foldK
          val mw           =
            middlewares
              .toList
              .sortBy(_.order)
              .map(m => m.middle _)
              .foldLeft[HttpRoutes[Task] => HttpRoutes[Task]](identity)((acc, m) => acc andThen m)
          val apiNames     = allHttpApis.map(_.name).mkString(",")
          BlazeServerBuilder[Task](cpuPool)
            .withHttpApp(mw(combinedApis).orNotFound)
            .bindHttp(8080, "0.0.0.0")
            .resource
            .evalTap(_ => log.info(s"Exposing $apiNames"))
        }
      )
}
