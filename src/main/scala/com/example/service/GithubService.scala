package com.example.service

import cats.data.EitherT
import cats.effect.Sync
import cats.syntax.either._
import com.example.common.CommonUtils
import com.example.config.{GithubConfig, GithubToken}
import com.example.entity.error.GithubApiError
import com.example.entity.payload.{Repository, User}
import io.circe.Decoder
import logstage.LogIO3
import org.http4s.Credentials.Token
import org.http4s._
import org.http4s.circe.CirceEntityDecoder._
import org.http4s.client.Client
import org.http4s.headers.Authorization
import org.http4s.util.CaseInsensitiveString
import zio.stream.Stream
import zio.{IO, Task, ZIO}

trait GithubService {
  def fetchRepos(org: String): Stream[GithubApiError, Repository]
  def fetchContributors(repo: Repository): Stream[GithubApiError, User]
}

object GithubService {

  final case class Impl(client: Client[Task], githubConf: GithubConfig, token: GithubToken, log: LogIO3[ZIO])(implicit
      S: Sync[Task]
  ) extends GithubService {
    import com.example.entity.error.GithubApiError._

    private val headers: List[Header] = token.token match {
      case Some(tk) => Authorization(Token(CaseInsensitiveString("token"), tk)) :: githubConf.toHeaderList
      case None => githubConf.toHeaderList
    }

    def fetchRepos(org: String): Stream[GithubApiError, Repository] = {
      fetch[Repository](s"${githubConf.baseUrl}orgs/$org/repos")
    }

    def fetchContributors(repo: Repository): Stream[GithubApiError, User] = {
      fetch[User](repo.contributorsUrl)
    }

    private def fetch[A: Decoder](startUrl: String): Stream[GithubApiError, A] = {
      Uri.fromString(startUrl) match {
        case Left(e)    => Stream.fail(FailedToParseUri(githubConf.baseUrl, e.details))
        case Right(url) =>
          CommonUtils.autoPage(url) { next =>
            log.trace(s"Fetching data from url: $url") *>
              runReq[A](
                Request[Task]()
                  .withMethod(Method.GET)
                  .withUri(next)
                  .withHeaders(headers: _*)
              )
          }
      }
    }

    private def runReq[A: Decoder](request: Request[Task]): IO[GithubApiError, (Headers, List[A])] =
      client
        .run(request)
        .use { resp =>
          (resp.status.code match {
            case 204                      => EitherT.pure(Right(List()))
            case i if Status(i).isSuccess => resp.attemptAs[List[A]].map(Right(_))
            case 400                      => resp.attemptAs[BadRequestError].map(Left(_))
            case 401                      => resp.attemptAs[UnauthorizedError].map(Left(_))
            case 403                      => resp.attemptAs[ForbiddenError].map(Left(_))
            case 404                      => EitherT.pure(Left(NotFoundError(s"Provided resource is not found")))
            case 422                      => resp.attemptAs[UnprocessableEntityError].map(Left(_))
            case 423                      => resp.attemptAs[RateLimitExceededError].map(Left(_))
            case i                        => EitherT.pure(Left(UnhandledResponseError(s"Response with unknown error code", i)))
          })
            .fold(e => Left(JsonParsingError(e)), _.leftMap[GithubApiError](identity))
            .map(GHResp(_, resp.headers))
        }
        .catchAll(h => ZIO.succeed(GHResp(Left(UnknownError(h.toString)), Headers.empty)))
        .flatMap {
          case GHResp(Left(e), _)        => ZIO.fail(e)
          case GHResp(Right(v), headers) => ZIO.succeed((headers, v))
        }

    private final case class GHResp[A](resp: Either[GithubApiError, A], headers: Headers)
  }
}
