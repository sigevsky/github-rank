FROM hseeberger/scala-sbt:11.0.10_1.4.7_2.13.4


WORKDIR /opt/app/github-rank

# its meant to be git clone .. but repo is private
COPY . .

RUN sbt --debug "assembly"

FROM openjdk:11.0.10

WORKDIR /opt/app
COPY --from=0 /opt/app/github-rank/target/scala-2.13/github-rank-assembly-1.0.0.jar /opt/app/rank.jar

CMD ["java", "-jar", "rank.jar"]